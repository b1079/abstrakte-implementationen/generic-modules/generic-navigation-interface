package com.kahlkepartner.navigation_interface.here_navigation;

import com.kahlkepartner.here_maps.routing.model.RouterRoute;
import com.kahlkepartner.osrmrouting.model.data.Route;
import implementations.AbstractGenericRoute;
import interfaces.data.RouteInformation;
import model.geometry.GeoJSONLineString;
import model.utils.FlexiblePolylineDecoder;

import java.util.Map;

/**
 * Implementation for Here API of the GenericRoute
 */

public class HEREGenericRoute extends AbstractGenericRoute {

    private Route route = new Route();

    HEREGenericRoute(RouterRoute routingResponse) {
        var firstRoute = routingResponse;
        var summary = firstRoute.getSections().get(0).getTravelSummary();
        route.setDistance(summary.getLength());
        route.setDuration(summary.getDuration());
        if (!firstRoute.getSections().isEmpty()) {
            var polyline = firstRoute.getSections().get(0).getPolyline();
            route.setGeometry(FlexiblePolylineDecoder.decode(polyline));
        }


    }

    @Override
    public GeoJSONLineString getGeoJsonLineString() {

        return this.route.getGeometry();
    }

    @Override
    public Map<String, String> getMetadata() {
        return null;
    }

    @Override
    public RouteInformation getRouteInformation() {
        RouteInformation routeInformation = new RouteInformation();
        routeInformation.setDistance(route.getDistance());
        routeInformation.setTravelTime(route.getDuration());

        return routeInformation;
    }
}
