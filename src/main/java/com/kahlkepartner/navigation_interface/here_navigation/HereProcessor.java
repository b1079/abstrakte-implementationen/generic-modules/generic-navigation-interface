package com.kahlkepartner.navigation_interface.here_navigation;

import com.kahlkepartner.genericgeocodinginterface.here.HereGeocodingService;
import com.kahlkepartner.here_maps.routing.HereRouteService;
import implementations.ProcessingInterface;
import interfaces.GenericRoute;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
/**
 * Processor for HERE API
 */
@ConditionalOnExpression("${here.routeing:true} and ${here.geocoder:true}")
public class HereProcessor implements ProcessingInterface {
    @Autowired
    HereRouteService routeService;
    @Autowired
    HereGeocodingService hereGeocodingService;

    @Override
    public List<GenericRoute> processLatLn(GeoJSONPoint from, GeoJSONPoint to) throws Exception {
        var route = routeService.calculateRoute(from, to);
        List<GenericRoute> routes = new ArrayList<>();
        for (var item : route.getRoutes()) {
            routes.add(new HEREGenericRoute(item));
        }
        return routes;
    }

    @Override
    public List<GenericRoute> processAddress(Address from, Address to) throws Exception {
        var fromLocation = hereGeocodingService.getLocationInformation(from).getLocation();
        var toLocation = hereGeocodingService.getLocationInformation(to).getLocation();
        return this.processLatLn(fromLocation, toLocation);
    }
}
