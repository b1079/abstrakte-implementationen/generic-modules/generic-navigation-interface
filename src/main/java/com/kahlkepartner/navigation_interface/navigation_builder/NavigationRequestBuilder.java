package com.kahlkepartner.navigation_interface.navigation_builder;

import com.kahlkepartner.navigation_interface.navigation_builder.requestBuilder.EndpointBuilder;
import implementations.ProcessingInterface;
import interfaces.GenericRoute;
import interfaces.data.Address;
import com.kahlkepartner.navigation_interface.navigation_builder.requestBuilder.LatLnBuilder;
import lombok.AccessLevel;

import lombok.Getter;
import lombok.Setter;
import model.geometry.GeoJSONPoint;

import java.util.List;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
/**
 * Request builder for navigation requests. This class helps to setup the right parameters and to execute the navigation
 * requests.
 */
public class NavigationRequestBuilder {
    private Address addressFrom;
    private Address addressTo;
    private GeoJSONPoint from;
    private GeoJSONPoint to;


    public EndpointBuilder buildByAddress() {
        return new EndpointBuilder(this);
    }

    public LatLnBuilder buildByCoordinates() {
        return new LatLnBuilder(this);
    }

    public NavigationRequestBuilder setAddressBuilder(EndpointBuilder endpointBuilder) {
        this.setAddressFrom(endpointBuilder.getFrom());
        this.setAddressTo(endpointBuilder.getTo());
        this.from = null;
        this.to = null;
        return this;
    }

    public NavigationRequestBuilder setLatLnBuilder(LatLnBuilder latLnBuilder) {
        this.from = latLnBuilder.from;
        this.to = latLnBuilder.to;
        this.addressFrom = null;
        this.addressTo = null;
        return this;
    }

    public List<GenericRoute> execute(ProcessingInterface runner) throws Exception {
        if (addressFrom != null && addressTo != null) {
            return runner.processAddress(addressFrom, addressTo);
        } else if (from != null && to != null) {
            return runner.processLatLn(from, to);
        }
        throw new IllegalStateException("Settings were not set correctly");
    }


}
