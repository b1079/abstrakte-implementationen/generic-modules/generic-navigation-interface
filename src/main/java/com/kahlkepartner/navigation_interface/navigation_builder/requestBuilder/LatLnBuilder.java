package com.kahlkepartner.navigation_interface.navigation_builder.requestBuilder;

import com.kahlkepartner.navigation_interface.navigation_builder.NavigationRequestBuilder;
import lombok.Getter;
import model.geometry.GeoJSONPoint;
@Getter
/**
 * Builder for coordinate requests
 */
public class LatLnBuilder {
    public GeoJSONPoint from = new GeoJSONPoint();
    public GeoJSONPoint to = new GeoJSONPoint();
    private NavigationRequestBuilder navigationRequestBuilder;

    public LatLnBuilder(NavigationRequestBuilder navigationRequestBuilder) {
        this.navigationRequestBuilder = navigationRequestBuilder;
    }

    public LatLnBuilder setFromLongitude(double longitude){
        this.from.setLongitude(longitude);
        return this;
    }
    public LatLnBuilder setFromLatitude(double latitude){
        this.from.setLatitude(latitude);
        return this;
    }
    public LatLnBuilder setToLongitude(double longitude){
        this.to.setLongitude(longitude);
        return this;
    }
    public LatLnBuilder setToLatitude(double latitude){
        this.to.setLatitude(latitude);
        return this;
    }

    public NavigationRequestBuilder build(){
        return this.navigationRequestBuilder.setLatLnBuilder(this);
    }
}
