package com.kahlkepartner.navigation_interface.navigation_builder.requestBuilder;

import com.kahlkepartner.navigation_interface.navigation_builder.NavigationRequestBuilder;
import interfaces.data.Address;
import lombok.Data;

@Data
/**
 * Helper class to manage adress requests
 */
public class EndpointBuilder {
    Address from;
    Address to;
    private NavigationRequestBuilder navigationRequestBuilder;
    boolean inputFrom = false;

    public EndpointBuilder(NavigationRequestBuilder navigationRequestBuilder) {
        this.navigationRequestBuilder = navigationRequestBuilder;
    }

    public AddressBuilder setFrom() {
        return new AddressBuilder(this, true);
    }


    public AddressBuilder setTo() {
        return new AddressBuilder(this, false);
    }

    public NavigationRequestBuilder build() {
        navigationRequestBuilder.setAddressBuilder(this);

        return this.navigationRequestBuilder;
    }

}
