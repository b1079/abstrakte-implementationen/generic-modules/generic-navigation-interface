package com.kahlkepartner.navigation_interface.navigation_builder.requestBuilder;


import interfaces.data.Address;

/**
 * Builder for address requests
 */
public class AddressBuilder {

    public Address address = new Address();
    private EndpointBuilder requestBuilder;
    private boolean from;

    public AddressBuilder(EndpointBuilder endpointBuilder, boolean from) {
        this.from = from;
        this.requestBuilder = endpointBuilder;
    }

    public AddressBuilder setCity(String city) {
        address.setCity(city);
        return this;
    }

    public AddressBuilder setHouseNumber(String city) {
        address.setHouseNumber(city);
        return this;
    }


    public AddressBuilder setStreet(String street) {
        address.setStreet(street);
        return this;
    }

    public AddressBuilder setCountry(String country) {
        address.setCountry(country);
        return this;
    }

    public EndpointBuilder setAddress(Address address) {
        this.address = address;
        return this.build();
    }

    public EndpointBuilder build() {
        if (from)
            this.requestBuilder.from = this.address;
        else
            this.requestBuilder.to = this.address;
        return requestBuilder;
    }

}
