package com.kahlkepartner.navigation_interface.osrm;

import com.kahlkepartner.osrmrouting.model.RouteServiceResponse;
import com.kahlkepartner.osrmrouting.model.data.Route;
import implementations.AbstractGenericRoute;
import interfaces.data.RouteInformation;
import model.geometry.GeoJSONLineString;

import java.util.Map;

/**
 * Implementation of GenericRoute for OSRM responses
 */
public class OSRMGenericRoute extends AbstractGenericRoute {
    private Route route;

    private OSRMGenericRoute() {

    }

    public static OSRMGenericRoute byRouteResponse(RouteServiceResponse routeServiceResponse, int i) {
        var genericRoute = new OSRMGenericRoute();

        genericRoute.route = routeServiceResponse.getRoutes().get(i);
        return genericRoute;
    }

    @Override
    public GeoJSONLineString getGeoJsonLineString() {
        return this.route.getGeometry();
    }

    @Override
    public Map<String, String> getMetadata() {
        return null;
    }

    @Override
    public RouteInformation getRouteInformation() {
        RouteInformation routeInformation = new RouteInformation();
        routeInformation.setDistance(route.getDistance());
        routeInformation.setTravelTime(route.getDuration());
        return routeInformation;
    }


}
