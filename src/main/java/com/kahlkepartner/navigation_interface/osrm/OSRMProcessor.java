package com.kahlkepartner.navigation_interface.osrm;

import com.kahlkepartner.nominatimapi.services.SearchService;
import com.kahlkepartner.osrmrouting.api.RouteService;
import com.kahlkepartner.osrmrouting.model.data.Coordinates;
import implementations.ProcessingInterface;
import interfaces.GenericRoute;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
/**
 * OSRM Implementation von einem generischen Interfaces
 */

@ConditionalOnExpression("${nominatim.enabled:true} and ${osrm.enabled:true}")
public class OSRMProcessor implements ProcessingInterface {
    @Autowired
    RouteService routeService;
    @Autowired
    SearchService nominatimService;

    @Override
    public List<GenericRoute> processLatLn(GeoJSONPoint from, GeoJSONPoint to) {
        Coordinates fromCoords = new Coordinates();
        fromCoords.setLatitude(from.getLatitude() + "");
        fromCoords.setLongitude(from.getLongitude() + "");
        Coordinates toCoords = new Coordinates();
        toCoords.setLatitude(to.getLatitude() + "");
        toCoords.setLongitude(to.getLongitude() + "");

        var response = routeService.calculateRoute(fromCoords, toCoords);

        ArrayList<GenericRoute> routes = new ArrayList<>();
        for (int i = 0; i < response.getRoutes().size(); i++) {
            routes.add(OSRMGenericRoute.byRouteResponse(response, i));
        }
        return routes;
    }


    /**
     * @param from
     * @param to
     * @return null when a address not found || Array of Routes calculated by osrm
     */
    @Override
    public List<GenericRoute> processAddress(Address from, Address to) {
        var fromAddress = nominatimService.geocodeAddress(
                com.kahlkepartner.nominatimapi.model.Address.fromAddressModel(from));
        var toAddress = nominatimService.geocodeAddress(
                com.kahlkepartner.nominatimapi.model.Address.fromAddressModel(to));

        if (fromAddress.length == 0 || toAddress.length == 0)
            return null;

        GeoJSONPoint fromPoint = new GeoJSONPoint();
        fromPoint.setLongitude(Double.parseDouble(fromAddress[0].getLon()));
        fromPoint.setLatitude(Double.parseDouble(fromAddress[0].getLat()));

        GeoJSONPoint toPoint = new GeoJSONPoint();
        toPoint.setLongitude(Double.parseDouble(toAddress[0].getLon()));
        toPoint.setLatitude(Double.parseDouble(toAddress[0].getLat()));

        return processLatLn(fromPoint, toPoint);
    }

}
