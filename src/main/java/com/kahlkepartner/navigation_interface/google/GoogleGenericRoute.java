package com.kahlkepartner.navigation_interface.google;


import com.google.maps.model.DirectionsRoute;

import implementations.AbstractGenericRoute;
import interfaces.data.RouteInformation;
import model.geometry.GeoJSONLineString;
import model.geometry.GeoJSONPoint;

import java.util.Arrays;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Routing service for google implementations
 */

public class GoogleGenericRoute extends AbstractGenericRoute {
    DirectionsRoute route;

    private GoogleGenericRoute() {
    }

    public static GoogleGenericRoute byDirectionsRoute(DirectionsRoute route) {
        GoogleGenericRoute directionsRoute = new GoogleGenericRoute();
        directionsRoute.route = route;

        return directionsRoute;
    }

    @Override
    public GeoJSONLineString getGeoJsonLineString() {
        List<GeoJSONPoint> list = route.overviewPolyline.decodePath()
                .stream()
                .map(elm -> GeoJSONPoint.fromLngLat(elm.lng, elm.lat)).collect(Collectors.toList());

        return GeoJSONLineString.fromPointList(list);
    }

    @Override
    public Map<String, String> getMetadata() {
        return null;
    }

    @Override
    public RouteInformation getRouteInformation() {
        RouteInformation routeInformation = new RouteInformation();
        double travelTime = Arrays.stream(route.legs).mapToLong(elm -> elm.duration.inSeconds).sum();
        double distance = Arrays.stream(route.legs).mapToLong(elm -> elm.distance.inMeters).sum();
        routeInformation.setTravelTime(travelTime);
        routeInformation.setDistance(distance);

        return routeInformation;
    }
}
