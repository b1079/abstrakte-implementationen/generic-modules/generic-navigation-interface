package com.kahlkepartner.navigation_interface.google;

import com.kahlkepartner.googleapi.service.GoogleRouteService;
import implementations.ProcessingInterface;
import interfaces.GenericRoute;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
/**
 * Implementation for the processing of requests to the google api
 */
@ConditionalOnExpression("${google.enabled:true}")
public class GoogleProcessor implements ProcessingInterface {
    private final GoogleRouteService routeService;

    public GoogleProcessor(GoogleRouteService routeService) {
        this.routeService = routeService;
    }

    /**
     * @param from coords from
     * @param to coords to
     * @return 0-n possible GenericRoutes which google processed
     * @throws Exception
     */
    @Override
    public List<GenericRoute> processLatLn(GeoJSONPoint from, GeoJSONPoint to) throws Exception {
        var routes = routeService.getDirectionsByCoords(from, to).routes;
        return Arrays.stream(routes).map(GoogleGenericRoute::byDirectionsRoute).collect(Collectors.toList());
    }

    /**
     * @param from address from
     * @param to address to
     * @return 0-n possible GenericRoutes which google processed
     * @throws Exception
     */
    @Override
    public List<GenericRoute> processAddress(Address from, Address to) throws Exception {
        var routes = routeService.getDirections(from, to).routes;
        return Arrays.stream(routes).map(GoogleGenericRoute::byDirectionsRoute).collect(Collectors.toList());
    }
}
