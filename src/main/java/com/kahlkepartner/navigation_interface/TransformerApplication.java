package com.kahlkepartner.navigation_interface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "com.kahlkepartner.navigation_interface",
        "com.kahlkepartner.osrmrouting",
        "com.kahlkepartner.googleapi",
        "com.kahlkepartner.nominatimapi",
        "com.kahlkepartner.here_maps",
        "com.kahlkepartner.genericgeocodinginterface"
})
public class TransformerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransformerApplication.class, args);
    }

}
