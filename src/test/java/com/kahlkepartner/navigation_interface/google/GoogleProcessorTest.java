package com.kahlkepartner.navigation_interface.google;

import interfaces.GenericRoute;
import interfaces.data.Address;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GoogleProcessorTest {
    @Autowired
    GoogleProcessor googleProcessor;

    @Test
    public void test() throws Exception {
        Address address = new Address();
        address.setCity("Biebelnheim");
        address.setStreet("Gabsheimer Weg");
        address.setHouseNumber("9");
        address.setCountry("Deutschland");
        Address to = new Address();
        to.setCity("Mainz");
        to.setCountry("Deutschland");
        var res = googleProcessor.processAddress(address, to);
        for (GenericRoute route : res) {
            System.out.println(route.getRouteInformation());

            assertNotNull(route.getRouteInformation().getDistance());
            assertNotNull(route.getRouteInformation().getTravelTime());
        }
    }

}
