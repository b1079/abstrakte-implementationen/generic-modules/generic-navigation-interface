package com.kahlkepartner.navigation_interface.navigation_builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NavigationRequestBuilderTest {
    @Test
    public void test() {
        NavigationRequestBuilder navigationRequestBuilder = new NavigationRequestBuilder();
        navigationRequestBuilder.buildByAddress().setFrom()
                .setCity("Biebelnheim")
                .setCountry("Deutschland")
                .setStreet("Gabsheimer Weg 9")
                .setHouseNumber("9").build()
                .setTo().setCity("Alzey")
                .setCountry("Deutschland").build()
                .build();
        assertEquals(navigationRequestBuilder.getAddressFrom().getCity(), "Biebelnheim");
    }
}
