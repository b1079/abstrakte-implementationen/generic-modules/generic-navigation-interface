package com.kahlkepartner.navigation_interface.osrm;

import interfaces.data.Address;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OSRMProcessorTest {

    @Autowired
    OSRMProcessor processor;

    @Test
    public void testOSRM() {
        Address address = new Address();
        address.setCity("Biebelnheim");
        address.setStreet("Gabsheimer Weg");
        address.setHouseNumber("9");
        address.setCountry("Deutschland");
        Address to = new Address();
        to.setCity("Mainz");
        to.setCountry("Deutschland");
        var res = processor.processAddress(address,to);
        assertFalse(res.isEmpty());
    }

}
