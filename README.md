# Generic Navigation Interface: 

## Inhalt:
Enthält die Implementation der generischen Interfaces für die Apis:

* HERE Maps API
* Google Maps API
* OSRM Routing

## Setup
### HERE Routing API 
Um HERE Routing funktionsfähig zu bekommen müssen folgende Settings gesetzt werden:
* ``here.apikey`` mit einem validen APIKey für HereAPI
### Google Maps API
Um Google Maps funktionsfähig zu bekommen müssen folgende Settings gesetzt werden:
* ``google.apikey`` mit einem validen API Key für Google Maps und Geocoding
### OSRM Routing
* ``nominatim.url`` mit einem Endpoint für Nominatim. Default: ``http://localhost:5450`` 
* ``osrm.endpoint.url`` mit einem erreichbaren OSRM API URL. Default: ``http://localhost:5000``

Nominatim ist für das Geocoding von Adressen von nöten, da OSRM nur Koordinaten zu Koordinaten 
Routenberechnungen beherscht.
## API Limiting:

### OSRM Routing
OSRM Routing hat in sich selbst keine Ratelimits. Dennoch müssen für Anfragen über Adressen 
zuerst eine Anfrage an die Nominatim API gesendet werden, welche verfügbar sein muss.

### Google Maps
Google Maps hat ein Limit von 100 Anfragen/s. Die Google Maven Implementation 
versucht dieses selbstständig kontrollieren. 

### HERE Routing
Die HERE Routing API hat ein Ratelimit von 10 Anfragen/s. 
Die Geocoding API hat jedoch ein Limit von 5 Anfragen/s. Für eine Routenberechnung von Addressen ist es notwendig
zu erst eine Geocodierung vorzunehmen, hierfür wird falls nicht anders eingestellt das Geocoding von HERE genutzt.
